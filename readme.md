# Moleculer d1soft / TypeORM

TypeORM mixin for moleculer.  
Also includes some useful helpers.

## Install

```sh
npm add -E typeorm @moleculer-d1soft/core @moleculer-d1soft/typeorm
```

## Example

```ts
import { Service as MoleculerService, ServiceBroker, ServiceSettingSchema } from 'moleculer';
import { Service } from 'moleculer-decorators';
import { TypeORMMixin, DataSource, TypeORMMixinOptions } from '@bukusaya/typeorm';

interface SomeServiceSettings extends ServiceSettingSchema, RedisMixinOptions {}

@Service({
    name: 'some-service',
    version: 1,
    settings: {
      db: {
        type: 'postgres',
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST,
        username: process.env.DB_USER,
        database: process.env.DB_NAME,
        name: 'some-service',
        applicationName: 'some-service',
        entities: [/** Entity, AnotherEntity **/],
        synchronize: true,
        logging: true
      },

      $secureSettings: ['db.password', 'db.username']
    },
    mixins: [new TypeORMMixin()]
})
export default class SomeService extends MoleculerService<SomeServiceSettings> {
  protected connection?: DataSource;

	public constructor(public broker: ServiceBroker) {
		super(broker);
	}
}

```