import { DataSourceOptions } from 'typeorm';

export type DataSourceWithName = DataSourceOptions & {
  name: string;
}

export interface TypeORMMixinOptions {
  db: DataSourceWithName[];
}
