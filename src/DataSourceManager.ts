import {
  DataSource,
  DataSourceOptions,
} from 'typeorm';

export class DataSourceManager {
  private static instance: DataSourceManager;
  private dataSources: Map<string, DataSource> = new Map();

  constructor() {}

  public static getInstance(): DataSourceManager {
    if (!DataSourceManager.instance) {
      DataSourceManager.instance = new DataSourceManager();
    }
    
    return DataSourceManager.instance;
  }

  public async createDataSource(
    name: string,
    options: DataSourceOptions
  ): Promise<DataSource> {
    const source = await (new DataSource(options)).initialize();

    return this.dataSources
      .set(name, source)
      .get(name) as DataSource;
  }

  public getDataSource(name?: string): DataSource {
    const dataSource = this.dataSources.get(name ?? 'default');
    if (!dataSource) {
      throw new Error(`DataSource "${name}" not found`);
    }
    
    return dataSource;
  }

  public getActiveDataSources() {
    return this.map().filter((ds: DataSource) => ds.isInitialized);
  }

  public async destroyDataSources() {
    return Promise.all(
      Array.from(this.dataSources.values()).map((dataStore) =>
        dataStore.destroy()
      )
    )
      .then((data) => {
        console.log(`Destroyed (${data.length}) data sources connections`);
      })
      .catch((errors) => {
        console.log("Error: Destroying DataSources:", errors);
      });
  }

  private map() {
    return Array.from(this.dataSources.values());
  }
}
