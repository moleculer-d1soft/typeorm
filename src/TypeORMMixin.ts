import {
  Service,
  ServiceSchema,
} from 'moleculer';
import { DataSource } from 'typeorm';

import { DataSourceManager } from './DataSourceManager';
import { TypeORMMixinOptions } from './TypeORMMixinOptions';

export class TypeORMMixin implements Partial<ServiceSchema>, ThisType<Service> {
  public connection?: DataSource;
  public dataSourceManager?: DataSourceManager;
  
  private schema?: Partial<ServiceSchema<TypeORMMixinOptions>> & ThisType<Service>;

  public async started(): Promise<void> {
    const dataSources = this.schema?.settings?.db;
    if (!dataSources) {
      throw new Error(`DataSource list is empty`);
    }

    this.dataSourceManager = DataSourceManager.getInstance();

    for (const { name, ...source } of dataSources) {
      await this.dataSourceManager.createDataSource(
        name,
        source
      );
    }

    return Promise.resolve();
  }

  public async stopped(): Promise<void> {
    if (this.dataSourceManager) {
      await this.dataSourceManager.destroyDataSources();
    }

    return Promise.resolve();
  }
}
