import {
  EntityTarget,
  ObjectLiteral,
  Repository,
} from 'typeorm';

import { DataSourceManager } from '../DataSourceManager';

export function getRepository<E extends ObjectLiteral>(
  entity: EntityTarget<E>,
  dataSourceName?: string
): Repository<E> {
  return DataSourceManager.getInstance()
    .getDataSource(dataSourceName)
    .getRepository(entity);
}

export function getMongoRepository<E extends ObjectLiteral>(
  entity: EntityTarget<E>,
  dataSourceName?: string
): Repository<E> {
  return DataSourceManager.getInstance()
    .getDataSource(dataSourceName)
    .getMongoRepository(entity);
}

export function getTreeRepository<E extends ObjectLiteral>(
  entity: EntityTarget<E>,
  dataSourceName?: string
): Repository<E> {
  return DataSourceManager.getInstance()
    .getDataSource(dataSourceName)
    .getTreeRepository(entity);
}
