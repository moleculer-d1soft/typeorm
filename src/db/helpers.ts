import {
  EntityManager,
  EntityTarget,
  FindManyOptions,
  QueryFailedError,
} from 'typeorm';

import { SortDirection } from '@moleculer-d1soft/core';

export interface GetPageOptions {
  page: number;
  count: number;
  filters?: FindManyOptions['where'];
  order?: FindManyOptions['order'];
  relations?: FindManyOptions['relations'];
}

export const getPage = async <T>(
  manager: EntityManager,
  model: EntityTarget<T>,
  options: GetPageOptions
) => {
  return manager.find(
    model,
    Object.assign(
      {
        take: options.count,
        skip: (options.page - 1) * options.count,
      },
      options?.filters ? { where: options.filters } : {},
      options?.order ? { where: options.order } : {}
    )
  );
};

export type DatabaseSortDirection = Record<
  SortDirection,
  'ASC' | 'DESC' | 1 | -1
>;
export const DatabaseSortDirections: DatabaseSortDirection = {
  asc: 'ASC',
  desc: 'DESC',
};

export enum PsqlCode {
  KeyViolates = '23503',
  UniqueViolates = '23505',
}

export enum DatabaseErrors {
  Unknown = 0,
  DuplicateUniqueViolate = 1,
  KeyViolatesError = 2,
}

/**
 * Checks if typeorm throws unique key violates error
 *
 * @param error Any thing
 * @returns
 */
export function isUniqueViolatesError(error: unknown): boolean {
  return (
    (error as QueryFailedError).driverError?.code === PsqlCode.UniqueViolates
  );
}

/**
 * Checks if typeorm throws key violates error
 *
 * @param error Any thing
 * @returns
 */
export function isKeyViolatesError(error: unknown): boolean {
  return (error as QueryFailedError).driverError?.code === PsqlCode.KeyViolates;
}

export const parseDatabaseError = (error: QueryFailedError): DatabaseErrors => {
  if (isKeyViolatesError(error)) {
    return DatabaseErrors.DuplicateUniqueViolate;
  } else if (isUniqueViolatesError(error)) {
    return DatabaseErrors.KeyViolatesError;
  }

  return DatabaseErrors.Unknown;
};
