import {
  DataSource,
  DataSourceOptions,
} from 'typeorm';

import { DataSourceManager } from '../DataSourceManager';

export default async (options: DataSourceOptions): Promise<DataSource | undefined> => {
	try {
		return DataSourceManager.getInstance()
			.createDataSource('default', options);
	} catch (error) {
		console.error(error);
		return undefined;
	}
};